/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {

        //Get the button id and call method to go to its description when the user click on the button
        var buttonAngeles = document.getElementById("btangeles");
        buttonAngeles.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('angeles');});

        var buttonChicago = document.getElementById("btchicago");
        buttonChicago.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('chicago');});

        var buttonHamilton = document.getElementById("bthamilton");
        buttonHamilton.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('hamilton');});

        var buttonRaleigh = document.getElementById("btraleigh");
        buttonRaleigh.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('raleigh');});

        var buttonCanada = document.getElementById("btcanada");
        buttonCanada.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('canada');});

        var buttonBrazil = document.getElementById("btbrazil");
        buttonBrazil.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('brazil');});

        var buttonBrazilSam = document.getElementById("btbrazilsam");
        buttonBrazilSam.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('brazilsam');});

        var buttonAlemania = document.getElementById("btalemania");
        buttonAlemania.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('alemania');});

        var buttonEspana = document.getElementById("btespana");
        buttonEspana.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('espana');});

        var buttonUK = document.getElementById("btuk");
        buttonUK.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('uk');});

        var buttonItaly = document.getElementById("btitaly");
        buttonItaly.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('italy');});

        var buttonHungria = document.getElementById("bthungria");
        buttonHungria.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('hungria');});

        var buttonInternacional = document.getElementById("btinternational");
        buttonInternacional.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('international');});

        var buttonInternacional = document.getElementById("btinternational");
        buttonInternacional.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('international');});

        var buttonFrancia = document.getElementById("btfrancia");
        buttonFrancia.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('francia');});

        var buttonBelgica = document.getElementById("btbelgica");
        buttonBelgica.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('belgica');});

        var buttonChina = document.getElementById("btchina");
        buttonChina.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('china');});

        var buttonJapan = document.getElementById("btjapan");
        buttonJapan.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('japan');});

        var buttonIndia = document.getElementById("btindia");
        buttonIndia.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('india');});

        var buttonAustralia = document.getElementById("btustralia");
        buttonAustralia.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('australia');});

        var buttonSingapore = document.getElementById("btsingapore");
        buttonSingapore.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('singapore');});

        var buttonMorocco = document.getElementById("btmorocco");
        buttonMorocco.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('morocco');});

        var buttonUae = document.getElementById("btuae");
        buttonUae.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('uae');});

        var buttonDeviceInfo = document.getElementById("btnDeviceInfo");
        buttonDeviceInfo.addEventListener('click',function(){JSInterfaceLocationH.changeActivityScreen('deviceinfo');});

        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();
