
package com.liferay.javiercosiotestapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

    }

    /*
    * This method is used to change to the next activity
    * */
    public void launchMode1Screen(View view){
        Intent i = new Intent(MainActivity.this, LocationHybridActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        //finish();
    }
}
