package com.liferay.javiercosiotestapp;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import org.apache.cordova.CordovaActivity;

public class DescriptionHybridActivity extends CordovaActivity {

    WebView webwiew;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.locationhtml);
        //Get the webview
        webwiew = (WebView)findViewById(R.id.wvLocationHtml);

        // register class containing methods to be exposed to JavaScript
        webwiew.getSettings().setJavaScriptEnabled(true);
        webwiew.getSettings().setDomStorageEnabled(true);

        //Get the intent
        Intent intent = getIntent();
        //Get the parameter location
        String id = intent.getStringExtra("location");
        //Use the location id to choose the correct html file
        String url = "file:///android_asset/www/Ubicaciones _ Liferay_files/" + id + ".html";
        //load the html in the webview
        webwiew.loadUrl(url);

    }

}
