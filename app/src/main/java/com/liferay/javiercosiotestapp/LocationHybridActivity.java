package com.liferay.javiercosiotestapp;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import org.apache.cordova.*;

public class LocationHybridActivity extends CordovaActivity
{

    WebView webwiew;

    JavaScriptInterface JSInterfaceLocationH;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

        // enable Cordova apps to be started in the background
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }

        setContentView(R.layout.locationhtml);
        //get the webview
        webwiew = (WebView)findViewById(R.id.wvLocationHtml);

        //Enable Javascript and dom storage
        webwiew.getSettings().setJavaScriptEnabled(true);
        webwiew.getSettings().setDomStorageEnabled(true);

        // register class containing methods to be exposed to JavaScript
        JSInterfaceLocationH = new JavaScriptInterface(this);
        webwiew.addJavascriptInterface(JSInterfaceLocationH, "JSInterfaceLocationH");

        //Load the html file in the webview
        webwiew.loadUrl("file:///android_asset/www/index.html");

    }


    public class JavaScriptInterface {
        Context mContext;

        //Instantiate the interface and set the context
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void changeActivityScreen(String name){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(name.equals("deviceinfo")){//If  user click on get device info button
                        //Create new intent object with the packageContext and next screen(Activity).
                        Intent i = new Intent(LocationHybridActivity.this, DeviceInfoActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //launch activity
                        startActivity(i);
                    }else{//If user click in more details of a location
                        //Create new intent object with the packageContext and next screen(Activity).
                        Intent i = new Intent(LocationHybridActivity.this, DescriptionHybridActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //Insert location variable to recover this data in the next activity
                        i.putExtra("location",name);
                        //launch activity
                        startActivity(i);
                        //finish();
                    }

                }
            });

        }

    }
}
