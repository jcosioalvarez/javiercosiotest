package com.liferay.javiercosiotestapp;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import com.liferay.utilities.Device;

import org.apache.cordova.CordovaActivity;

public class DeviceInfoActivity extends CordovaActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.deviceinfo);
        //Get the webview
        WebView webwiew = (WebView) findViewById(R.id.wvdeviceinfo);

        // register class containing methods to be exposed to JavaScript
        webwiew.getSettings().setJavaScriptEnabled(true);
        webwiew.getSettings().setDomStorageEnabled(true);

        //Get the TextView
        TextView tvDeviceInfo = (TextView) findViewById(R.id.tvdeviceinfo);

        //Create the device object
        Device device = new Device();

        //Get data from device object
        String osVersion = device.getOSVersion();
        String sdkVersion = device.getSDKVersion();
        String serialNumber = device.getSerialNumber();
        String timeZone = device.getTimeZoneID();

        //Put the text in the TextView
        String message = "OS: " + osVersion + ", SDK: " + sdkVersion + " , Serial Number: " + serialNumber + " , Timezone: " + timeZone;
        tvDeviceInfo.setText(message);

        //load the html in the webview
        webwiew.loadUrl("file:///android_asset/www/Ubicaciones _ Liferay_files/deviceinfo.html");

    }


}

