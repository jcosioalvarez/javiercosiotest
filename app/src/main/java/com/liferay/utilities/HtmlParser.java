package com.liferay.utilities;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.*;
import com.liferay.pojos.Location;

import java.io.IOException;
import java.util.ArrayList;

/*
This class is used to parse the HTML file of Liferay locations website
 */
public class HtmlParser {

    public ArrayList<Location> getLocations(){

        ArrayList<Location> locationList = new ArrayList<Location>();
        try {
            //Get the HTML with the locations
            Document doc = Jsoup.connect("https://www.liferay.com/es/locations").get();
            //Extract the div.office contect
            Elements locationDiv = doc.select("div.office");

            for (Element src : locationDiv) {
                //H2 inside div.office are all the location names
                String addressLocality = src.getElementsByTag("h2").text();

                //Get all div content
                String address = src.getElementsByTag("div").text();

                //Create location object
                Location location = new Location(addressLocality, "", address);
                //Add location object to list
                locationList.add(location);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        //Return the ArrayList
        return locationList;
    }

}
