










AUI.add(
	'portal-available-languages',
	function(A) {
		var available = {};

		var direction = {};

		

			available['en_US'] = 'ingl�s (Estados Unidos)';
			direction['en_US'] = 'ltr';

		

			available['ar_SA'] = '�rabe (Arabia Saudita)';
			direction['ar_SA'] = 'rtl';

		

			available['de_DE'] = 'alem�n (Alemania)';
			direction['de_DE'] = 'ltr';

		

			available['en_AU'] = 'ingl�s (Australia)';
			direction['en_AU'] = 'ltr';

		

			available['es_ES'] = 'espa�ol (Espa�a)';
			direction['es_ES'] = 'ltr';

		

			available['fi_FI'] = 'fin�s (Finlandia)';
			direction['fi_FI'] = 'ltr';

		

			available['fr_FR'] = 'franc�s (Francia)';
			direction['fr_FR'] = 'ltr';

		

			available['hr_HR'] = 'croata (Croacia)';
			direction['hr_HR'] = 'ltr';

		

			available['hu_HU'] = 'h�ngaro (Hungr�a)';
			direction['hu_HU'] = 'ltr';

		

			available['it_IT'] = 'italiano (Italia)';
			direction['it_IT'] = 'ltr';

		

			available['ja_JP'] = 'japon�s (Jap�n)';
			direction['ja_JP'] = 'ltr';

		

			available['nb_NO'] = 'bokmal noruego (Noruega)';
			direction['nb_NO'] = 'ltr';

		

			available['nl_NL'] = 'neerland�s (Holanda)';
			direction['nl_NL'] = 'ltr';

		

			available['pl_PL'] = 'polaco (Polonia)';
			direction['pl_PL'] = 'ltr';

		

			available['pt_BR'] = 'portugu�s (Brasil)';
			direction['pt_BR'] = 'ltr';

		

			available['ru_RU'] = 'ruso (Rusia)';
			direction['ru_RU'] = 'ltr';

		

			available['sv_SE'] = 'sueco (Suecia)';
			direction['sv_SE'] = 'ltr';

		

			available['zh_CN'] = 'chino (China)';
			direction['zh_CN'] = 'ltr';

		

		Liferay.Language.available = available;
		Liferay.Language.direction = direction;
	},
	'',
	{
		requires: ['liferay-language']
	}
);