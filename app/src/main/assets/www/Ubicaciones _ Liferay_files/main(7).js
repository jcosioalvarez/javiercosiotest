AUI().ready(
	'anim',
	'class-toggle',
	'event-onscreen',
	'yui-throttle',
	function(A) {
		var BODY = A.getBody();

		var WIN = A.getWin();

		var bannerHeight = 0;

		var banner = A.one('#banner');

		if (banner) {
			bannerHeight = banner.get('clientHeight');
		}

		var triggerPos = 200;

		BODY.delegate(
			'click',
			function(event) {
				var node = event.currentTarget;

				var section = A.one(node.get('hash'));

				if (!section) {
					return;
				}

				event.preventDefault();

				var offset = parseInt(node.attr('data-offset'), 10);

				var scrollTo = parseInt(section.getY(), 10);

				if ((scrollTo - window.scrollY) < triggerPos) {
					scrollTo -= bannerHeight;
				}

				if (offset) {
					scrollTo -= offset;
				}

				new A.Anim(
					{
						duration: 0.5,
						easing: 'easeBoth',
						node: 'win',
						to: {
							scroll: [0, scrollTo]
						}
					}
				).run();

				window.history.pushState('', '', node.get('hash'));
			},
			'.animate-scroll'
		);

		A.all('.lazy-load').each(
			function(node) {
				node.on(
					'onscreen',
					function(event) {
						var datasrc = node.attr('data-src');
						var src = node.attr('src');

						if (src != datasrc) {
							node.attr('src', datasrc);
						}

						node.addClass('lazy-loaded');

						node.detach();
					}
				);
			}
		);

		A.all('.on-screen-helper').each(
			function(node) {
				node.on(
					'onscreen',
					function(event) {
						node.addClass('on-screen');

						if (!node.attr('data-repeat')) {
							node.detach();
						}
					}
				);

				node.on(
					'onscreentop',
					function(event) {
						node.addClass('on-screen-top');

						if (!node.attr('data-repeat')) {
							node.detach();
						}
					}
				);

				node.on(
					['offscreenbottom', 'offscreentop'],
					function(event) {
						if (node.attr('data-repeat')) {
							node.removeClass('on-screen');
						}

						if (node.attr('data-repeat-top')) {
							node.removeClass('on-screen-top');
						}
					}
				);
			}
		);

		if (A.ClassToggle) {
			new A.ClassToggle().render();
		}

		var clearMobileNav = function() {
			if (window.innerWidth < Liferay.BREAKPOINTS.TABLET) {
				var dropdownContentNodes = A.all('.dropdown-content');

				dropdownContentNodes.removeAttribute('style');
			}
		};

		A.on(
			'resize',
			function() {
				A.throttle(clearMobileNav(), 250);
			}
		);

		var lastScrollPos = 0;
		var savedScrollPos = 0;

		var displayBanner = function() {
			var scrollPos = WIN.get('docScrollY');

			if (scrollPos > lastScrollPos) {
				savedScrollPos = scrollPos;
			}

			lastScrollPos = scrollPos;

			if (scrollPos < (savedScrollPos - 100)) {
				savedScrollPos = scrollPos + 100;

				BODY.removeClass('hide-banner');
			}
			else if (scrollPos > triggerPos) {
				BODY.addClass('hide-banner');
			}
			else {
				BODY.removeClass('hide-banner');
			}
		};

		A.on(
			'scroll',
			function() {
				A.throttle(displayBanner(), 250);
			}
		);

		var standardSpacing = 16;

		function toggleAccordion(event) {
			var currentTarget = event.currentTarget;

			if (window.innerWidth < Liferay.BREAKPOINTS.TABLET) {
				var dropdownContent = currentTarget.one('.accordion-toggle-content');

				if (dropdownContent.get('offsetHeight') > 0) {
					dropdownContent.removeAttribute('style');

				}
				else {
					dropdownContent.setStyle('height', (dropdownContent.get('scrollHeight') + standardSpacing) + 'px');
				}
			}
		}

		banner.delegate('click', toggleAccordion, '.accordion-toggle');

		var toggleSearch = function(event) {
			banner.toggleClass('search-active');

			if (banner.hasClass('search-active')) {
				var searchInput = banner.one('.doc-search-input');

				if (searchInput) {
					searchInput.focus();
					searchInput.select();
				}
			}
		};

		banner.delegate('click', toggleSearch, '.close-icon, .search-bar');
	}
);

window.addEventListener(
	'DOMContentLoaded',
	function() {
		var cookieAcceptance = document.querySelector('.cookie-acceptance');
		var cookieAcceptanceBtn = document.querySelector('.cookie-acceptance-btn');

		if (cookieAcceptance && cookieAcceptanceBtn) {
			cookieAcceptanceBtn.addEventListener(
				'click',
				function(event) {
					var expirationDate = new Date();

					expirationDate.setYear(expirationDate.getFullYear() + 1);

					document.cookie = 'OSB_WWW_GDPR_ACCEPTED=true; expires=' + expirationDate.toUTCString();

					cookieAcceptance.classList.add('hide');
				}
			);
		}
	}
);