var animatedDropdown = (
	function() {
		var activeClassName = 'active-animated-dropdown';
		var animatedDropdownNode;
		var animatedDropdownContainerNode;
		var bannerNode = document.querySelector('#banner');

		var navItemParentNodes = bannerNode.querySelectorAll('.primary-nav .nav-item-parent');
		var primaryNavNode = bannerNode.querySelector('.primary-nav');

		var initAnimatedDropdown = function() {
			animatedDropdownNode = document.createElement('div');
			animatedDropdownContainerNode = document.createElement('div');

			animatedDropdownNode.className = 'animated-dropdown bottom dropdown-content right';
			animatedDropdownContainerNode.className = 'animated-dropdown-container';

			animatedDropdownContainerNode.appendChild(animatedDropdownNode);

			document.querySelector('body').appendChild(animatedDropdownContainerNode);

			var animationDelay = 10;
			var animationTimeout;

			navItemParentNodes.forEach(
				function(node) {
					node.addEventListener(
						'mouseenter',
						function(e) {
							if (!isMobile()) {
								var dropdownContentNode = e.currentTarget.querySelector('.dropdown-content');

								moveDropdown(dropdownContentNode);

								animationTimeout = setTimeout(
									function() {
										primaryNavNode.classList.add(activeClassName);
										animatedDropdownContainerNode.classList.add(activeClassName);
									},
									animationDelay
								);
							}
						}
					);
				}
			);

			primaryNavNode.addEventListener(
				'mouseleave',
				function() {
					clearTimeout(animationTimeout);

					animatedDropdownContainerNode.classList.remove(activeClassName);
					primaryNavNode.classList.remove(activeClassName);
				}
			);
		};

		var isMobile = function() {
			return window.innerWidth < Liferay.BREAKPOINTS.TABLET;
		};

		var moveDropdown = function(dropdownContentNode) {
			var dropdownContentCoordinates = dropdownContentNode.getBoundingClientRect();

			animatedDropdownContainerNode.style.transform = 'translate(' + (dropdownContentCoordinates.left) + 'px, ' + dropdownContentCoordinates.top + 'px)';
			animatedDropdownContainerNode.style.width = dropdownContentCoordinates.width + 'px';
			animatedDropdownNode.style.width = dropdownContentCoordinates.width + 'px';
			animatedDropdownNode.style.height = dropdownContentCoordinates.height + 'px';
		};

		return {
			initAnimatedDropdown: initAnimatedDropdown
		};
	}
)();

animatedDropdown.initAnimatedDropdown();